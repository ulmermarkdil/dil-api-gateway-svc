package com.agora.brokerage.dil.apiGateway.filters;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

/**
 * Relays request attribute values as headers to back-end services.
 * 
 * Logging of the header value is controlled by the sensitive attribute. Filters marked sensitive
 * will omit the value from the logging
 * 
 * @author Mark Ulmer
 */
public class RequestAttributeRelayFilter extends ZuulFilter {
  private static final Logger log = LoggerFactory.getLogger(RequestAttributeRelayFilter.class);

  private final String attributeName;
  private final String headerName;
  private final boolean sensitive;

  public RequestAttributeRelayFilter(String attributeName, String headerName, boolean sensitive) {
    Validate.notNull(attributeName, "A attributeName is required.");
    Validate.notNull(headerName, "A headerName is required.");
    this.attributeName = attributeName;
    this.headerName = headerName;
    this.sensitive = sensitive;
    log.info("{}", this);
  }

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 10;
  }

  /**
   * This filter should execute if the request has the attribute present
   */
  @Override
  public boolean shouldFilter() {
    RequestContext context = RequestContext.getCurrentContext();
    HttpServletRequest request = context.getRequest();
    Object attr = request.getAttribute(this.attributeName);
    boolean shouldFilter = (null != attr);
    log.debug("{} shouldFilter=[{}]", this, shouldFilter);
    return shouldFilter;
  }

  /**
   * Run the filter and relay attribute as a header
   */
  @Override
  public Object run() {
    RequestContext context = RequestContext.getCurrentContext();

    String logValue =
        this.sensitive ? "Omitted" : (String) context.getRequest().getAttribute(this.attributeName);
    log.info("Relaying Attribute [{}],Header [{}],Value [{}]", this.attributeName, this.headerName,
        logValue);
    context.addZuulRequestHeader(this.headerName,
        (String) context.getRequest().getAttribute(this.attributeName));
    return null;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("[attributeName=");
    builder.append(attributeName);
    builder.append(", headerName=");
    builder.append(headerName);
    builder.append(", sensitive=");
    builder.append(sensitive);
    builder.append("]");
    return builder.toString();
  }

}
