package com.agora.brokerage.dil.apiGateway;

import javax.servlet.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.util.StringUtils;
import com.agora.brokerage.dil.apiGateway.filters.RequestAttributeRelayFilter;
import com.agora.brokerage.dil.apiGateway.filters.UriWhitelistFilter;
import com.netflix.servo.util.Strings;

/**
 * Spring Security Configuration for the DIL API Gateway Microservice
 * 
 * @see RequestAttributeRelayFilter
 * @see UriWhitelistFilter
 * 
 * @author mark.ulmer@capco.com
 *
 */
@EnableWebSecurity
public class SpringSecurityConfigurer extends GlobalAuthenticationConfigurerAdapter {

  private static final String DIL_AUTH_SVC_X_AUTH_TOKEN = "x-auth-token";

  static Logger log = LoggerFactory.getLogger(SpringSecurityConfigurer.class);

  private final GatewayAppConfiguration gatewayConfig;

  private String managementContextPath;

  private String[] managementSecurityRoles;

  @Autowired
  public SpringSecurityConfigurer(GatewayAppConfiguration gatewayConfig,
      @Value("${management.context-path:/}") String managementContextPath,
      @Value("${management.security.roles: #{null}}") String managementSecurityRoles) {
    this.gatewayConfig = gatewayConfig;
    this.managementContextPath = managementContextPath;
    if (StringUtils.hasText(managementSecurityRoles)) {
      this.managementSecurityRoles = managementSecurityRoles.split(",");
    } else {
      this.managementSecurityRoles = new String[0];
    }

  }

  @Override
  public void init(AuthenticationManagerBuilder auth) throws Exception {
    log.info("{}", "init called");
  }

  @Bean
  public ApplicationSecurity applicationSecurity() {
    return new ApplicationSecurity();
  }

  /**
   * Setup white listing filter
   * 
   * @return
   */
  private UriWhitelistFilter uriWhitelistFilter() {

    if (this.isWhitelistConfigured()) {
      log.info("Whitelist configured for filterPath:[{}] uris:[{}]",
          this.gatewayConfig.getWhitelist().getFilterPath(),
          this.gatewayConfig.getWhitelist().getUris());
      return new UriWhitelistFilter(this.gatewayConfig.getWhitelist().getFilterPath(),
          this.gatewayConfig.getWhitelist().getUris());
    } else {
      log.warn("*********NO WHITELIST CONFIGURATION DEFINED.  PROCEEDING WITH START-UP*********");
      return null;
    }
  }

  /**
   * Returns true if whitelist information is detected in the configuration
   * 
   * @return
   */
  private boolean isWhitelistConfigured() {
    return !Strings.isNullOrEmpty(this.gatewayConfig.getWhitelist().getFilterPath())
        && this.gatewayConfig.getWhitelist().getUris() != null;
  }

  /**
   * Application Security setup of Spring WebSecurityConfigurerAdapter
   * 
   * @author bradesco
   *
   */
  protected class ApplicationSecurity extends WebSecurityConfigurerAdapter {
    ApplicationSecurity() {}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      GatewayAppConfiguration gatewayConfig = getGatewayConfig();
      log.trace("configure called");

      if (null == managementContextPath || managementContextPath.trim().equals("")) {
        // This class will secure ALL endpoints other than those in permitAll
        // if the management context path is not set properly.
        managementContextPath = "/**";
      } else {
        managementContextPath = managementContextPath + "/**";
      }

      ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry =
          http.httpBasic().and().csrf().disable().authorizeRequests();

      for (AclConfig acl : gatewayConfig.getAcls()) {
        log.info("Loading ACL: method:[{}], pattern:[{}], role:[{}]}", acl.getMethod(),
            acl.getPattern(), acl.getRole());
        registry = registry.antMatchers(HttpMethod.valueOf(acl.getMethod()), acl.getPattern())
            .hasRole(acl.getRole());
      }

      if (gatewayConfig.getPermitAllUrls().length > 0) {
        log.info("permitAll setup: {}", (Object)gatewayConfig.getPermitAllUrls());
        registry.antMatchers(gatewayConfig.getPermitAllUrls()).permitAll();
      } else {
        log.warn("skipping permitAll setup.  Configuration missing");
      }
      // apply security to management endpoints, not in the permit-all-urls
      if (managementSecurityRoles.length > 0) {
        log.info("management endpoint security setup: {}", (Object)managementSecurityRoles);
        registry.antMatchers(managementContextPath).hasAnyAuthority(managementSecurityRoles);
      } else {
        log.warn("skipping management endpoint security setup.  Configuration missing");

      }

      // Insert the whitelist filter before the security filter
      // to prevent access to URLs that are not meant for public
      // consumption.

      Filter whitelist = uriWhitelistFilter();
      if (whitelist != null) {
        http.addFilterBefore(whitelist, FilterSecurityInterceptor.class);
      }

    }
  }

  public GatewayAppConfiguration getGatewayConfig() {
    return gatewayConfig;
  }
}
