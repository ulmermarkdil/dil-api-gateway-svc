package com.agora.brokerage.dil.apiGateway.filters;

import java.io.IOException;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Can be wired up in a servlet filter chain to prohibit access to all but the whitelisted URIs. The
 * filter may be limited to a single ant pattern path to limit the URIs that are affected by this
 * filter. To filter on every URI set filterPath = "/**".
 * 
 * @author Mark Ulmer
 */
public class UriWhitelistFilter extends OncePerRequestFilter {
  private static final Logger log = LoggerFactory.getLogger(UriWhitelistFilter.class);

  private String filterPath;
  private List<String> whitelistedUris;
  private AntPathMatcher matcher;

  public UriWhitelistFilter(String filterPath, List<String> whitelistedUris) {
    this.filterPath = filterPath;
    this.whitelistedUris = whitelistedUris;
    this.matcher = new AntPathMatcher();

    Validate.notNull(this.filterPath, "A filterPath is required.");
    Validate.notNull(this.whitelistedUris, "A list of whitelisted URIs is required");
    Validate.notNull(this.matcher, "An AntPathMatcher is required.");
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {

    String requestedUri = request.getRequestURI();

    // Determine if this URI should be filtered by matching against the
    // filterPath.
    if (!uriIsFiltered(requestedUri)) {
      log.debug("URI [{}] is not in filterPath [{}].  Continuing filter chain execution.",
          requestedUri, filterPath);

      // The URI is not to be filtered. Process the chain normally.
      filterChain.doFilter(request, response);
      return;
    }

    if (!uriIsWhitelisted(requestedUri)) {
      // If the requested URI is not in the list of whitelisted URIs then
      // return a not found error and cease chain execution.
      log.info("URI [{}] is not whitelisted.  Sending a 404 response in return.", requestedUri);

      response.setStatus(HttpServletResponse.SC_NOT_FOUND);
      return;

    } else {
      // The URI is in the list of whitelisted URIs so proceed.
      log.debug("URI [{}] is whitelisted.  Continuing filter chain execution.", requestedUri);

      filterChain.doFilter(request, response);
    }
  }

  /**
   * @param uri
   * @return Whether the URI should be filtered by this servlet filter.
   */
  private boolean uriIsFiltered(String uri) {
    return matcher.match(filterPath, uri);
  }

  /**
   * @param uri
   * @return Whether the URI is whitelisted.
   */
  private boolean uriIsWhitelisted(String uri) {
    boolean whitelisted = false;

    for (String whitelistPattern : whitelistedUris) {
      if (matcher.match(whitelistPattern, uri)) {
        whitelisted = true;
        break;
      }
    }

    return whitelisted;
  }
}
