package com.agora.brokerage.dil.apiGateway;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import com.agora.brokerage.dil.apiGateway.filters.RequestAttributeRelayFilter;
import com.netflix.servo.util.Strings;

@ConfigurationProperties(prefix = "dil-api-gateway-svc")
@Configuration
public class GatewayAppConfiguration {

  @Value("${dil-api-gateway-svc.session.attribute.name:com.dil.session.x-auth-token}")
  private String sessionAttribute;

  @Value("${dil-api-gateway-svc.session.header.name:X-Auth-Token}")
  private String sessionHeader;

  @Value("${dil-api-gateway-svc.session.attribute.name:com.dil.session.x-auth-user-token}")
  private String userAttribute;

  @Value("${dil-api-gateway-svc.session.header.name:X-Auth-User-Token}")
  private String userHeader;

  private String[] permitAllUrls;

  private List<AclConfig> acls = new ArrayList<AclConfig>();

  private WhitelistConfig whitelist = new WhitelistConfig();

  public GatewayAppConfiguration(
      @Value("${dil-api-gateway-svc.permit-all-urls:#{null}}") String permitAllUrls) {

    if (StringUtils.hasText(permitAllUrls)) {
      this.permitAllUrls = permitAllUrls.split(",");
    } else {
      this.permitAllUrls = new String[0];
    }

    this.getAcls();
    this.getWhitelist();
  }

  public List<AclConfig> getAcls() {
    return acls;
  }

  public void setAcls(List<AclConfig> acls) {
    this.acls = acls;
  }

  public String getSessionAttribute() {
    return sessionAttribute;
  }

  public String getSessionHeader() {
    return sessionHeader;
  }

  public String getUserAttribute() {
    return userAttribute;
  }

  public String getUserHeader() {
    return userHeader;
  }

  public String[] getPermitAllUrls() {
    return permitAllUrls;
  }

  public WhitelistConfig getWhitelist() {
    return whitelist;
  }

  public void setWhitelist(WhitelistConfig whitelist) {
    this.whitelist = whitelist;
  }

  // @Bean
  // public SessionRepository<?> sessionRepository() {
  // JedisConnectionFactory factory = new JedisConnectionFactory();
  // return new RedisOperationsSessionRepository(factory);
  // }

  /**
   * Following bean modifies Spring MVC Rest Controllers and allows a PUT of a Multipart File
   * 
   * @return
   */
  @Bean
  public MultipartResolver multipartResolver() {
    return new StandardServletMultipartResolver() {
      @Override
      public boolean isMultipart(HttpServletRequest request) {
        String method = request.getMethod().toLowerCase();
        // By default, only POST is allowed. Since this is an 'update' we should accept PUT.
        if (!Arrays.asList("put", "post").contains(method)) {
          return false;
        }
        String contentType = request.getContentType();
        return (contentType != null && contentType.toLowerCase().startsWith("multipart/"));
      }
    };
  }

  /**
   * Sets up Zuul filter to relay the Session attribute downstream as a header
   * 
   * @return
   */
  @Bean
  public RequestAttributeRelayFilter sessionRelayFilter() {
    return new RequestAttributeRelayFilter(this.sessionAttribute, this.sessionHeader, true);
  }

  /**
   * Sets up Zuul filter to relay the User attribute downstream as a header
   * 
   * @return
   */
  @Bean
  public RequestAttributeRelayFilter userRelayFilter() {
    return new RequestAttributeRelayFilter(this.userAttribute, this.userHeader, false);
  }

  public class WhitelistConfig {
    private String filterPath;
    private List<String> uris = new ArrayList<String>();

    public WhitelistConfig() {
      this.getFilterPath();
      this.getUris();
    }

    public String getFilterPath() {
      return filterPath;
    }

    public void setFilterPath(String filterPath) {
      this.filterPath = filterPath;
    }

    public List<String> getUris() {
      return uris;
    }

    public void setUris(List<String> uris) {
      this.uris = uris;
    }
  }

}
